# Federated FAIR Data Space Documentation

FAIR Federated Data Space - A space to federate them all.This solution should enable easy alignment of the metadata to provide a unified search and data retrieval over multiple data sets from heterogeneous and distributed community-specific repo.

The proposed solution is built by integrating together existing and state of the art tools supporting the data fairification process. Our goal is to create an innovative solution which allows data repositories to easily register their resources, FAIRify their data description, publish and share FAIR datasets within the federated data space. Our approach offers a generic framework that should be easily deployed and reused for any purpose. To achieve this objective, we are taking advantage of professional devOps tools (containers and Kubernetes) to ease deployment and management on any infrastructure and for any purpose.


Files for Kubernetes deployment:

- https://dci-gitlab.cines.fr/dad/fdp-pillar

Graphical interface allowing data repository owners to easily publish their datasets:

- https://dci-gitlab.cines.fr/dad/ffds-register-front
- https://dci-gitlab.cines.fr/dad/ffds-register-api

FAIR Data Point:

- https://dci-gitlab.cines.fr/dad/fdp-client
- https://dci-gitlab.cines.fr/dad/fdp-api

SmartApi:

- https://dci-gitlab.cines.fr/dad/smartapi

Ffds smart harvester api

- https://dci-gitlab.cines.fr/dad/ffds-smart-harvester